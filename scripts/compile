#!/usr/bin/env bash
#
# Compile Onion Services docs into the docs folder.
#

# Parameters
DIRNAME="`dirname $0`"
BASEDIR="$DIRNAME/.."
DOCS="docs"
REPOS="repos"
BASE_GIT_URL="https://gitlab.torproject.org/tpo/onion-services"
PROJECTS="onionplan:research oniongroove:apps/web/oniongroove onionspray:apps/web/onionspray"
PROJECTS="$PROJECTS onionprobe:apps/web/onionprobe onionmine:apps/web/onionmine"
PROJECTS="$PROJECTS onion-launchpad:apps/web/onion-launchpad onionbalance:apps/base/onionbalance"

# Clone or update a given repository
function clone_or_update {
  local repo="$1"
  local path="$2"
  local dir
  local base

  # Clone or update
  if [ ! -d "$REPOS/$repo" ]; then
    echo "Cloning $BASE_GIT_URL/${repo}.git into $REPOS/$repo..."
    git clone --depth 1 $BASE_GIT_URL/${repo}.git $REPOS/$repo
  else
    # Cleanup unstagged changes and update from upstream
    echo "Updating $REPOS/$repo..."
    (
      cd $REPOS/$repo &> /dev/null
      git restore .
      git pull
    )
  fi

  # Symlink the docs/ folder
  dir="`dirname $path`"
  base="`basename $path`"
  mkdir -p $DOCS/$dir
  rm -f $DOCS/$path
  ln -r -s -f -t $DOCS/$dir $REPOS/$repo/docs
  mv $DOCS/$dir/docs $DOCS/$dir/$base
}

# Iterate over all docs
for item in $PROJECTS; do
  repo="`echo $item | cut -d : -f 1`"
  path="`echo $item | cut -d : -f 2`"

  # Clone or update repository
  clone_or_update $repo $path
done

# Remove unneeded files that might be broken symlinks and which may break
# MkDocs compilation
for link in favicon.png tor-logo1x.png site.css; do
  find $REPOS -name $link -exec rm {} \;
done

# Set PIPFILE env
export PIPENV_PIPFILE=$BASEDIR/vendors/onion-mkdocs/Pipfile

# Install custom Pipenv plugins
# These aren't upstreamed, and we're using upstream's Pipfile.lock
# So we install manually here
#pipenv install mkdocs-minify-plugin mkdocs-linkcheck
