# Onion Service implementations

Considering to add Onion Service support in your application?

This page may help you choose the right implementation or to migrate between
[implementations](../../apps/base/README.md):

* The legacy Tor implementation, called "[C Tor][]" or "little tor", is a tool
  that's battle tested and extremely stable.
* For the long run, the Tor core software is being rewritten to be safer,
  easier to use, maintain and understand. This new implementation already
  exists and it's called "[Arti][]".

[C Tor]: https://gitlab.torproject.org/tpo/core/tor
[Arti]: https://arti.torproject.org

Last updated on 2025-02-19.

## Feature matrix

### Onion Service clients

Feature                                 | Specification                                | Status on [C Tor][]                            | Status on [Arti][]                                   | Config on [C Tor][]    | Config on [Arti][]
-------------------------------         | -------------                                | -------------------                            | ------------------                                   | -------------------    | -------------------
Vanguards (lite and full)               | [vanguards-spec][], [prop333][], [prop292][] | :white_check_mark: Implemented                 | :white_check_mark: Implemented ([1.2.2][arti-1.2.2]) | `VanguardsLiteEnabled` | `vanguards`
Restricted discovery[^restricted-disc]  | [rend-spec][]                                | :white_check_mark: Implemented                 | :white_check_mark: Implemented ([1.2.7][arti-1.2.7]) | `ClientOnionAuthDir`   | `restricted_mode`
Proof of Work protection                | [prop327][]                                  | :white_check_mark: Implemented (0.4.8.1-alpha) | :white_check_mark: [Implemented][pow-arti-client] ([1.3.0][arti-1.3.0]) | N/A                    | N/A
Conflux                                 | [prop329][]                                  | [Planned][conflux-c-tor]                       | :x: Not implemented                                  | N/A                                                             | N/A
Debian package[^package]                | [Debian Policy][]                            | :white_check_mark: [Implemented][debs]         | [Underway][artipack] ([1.2.7][arti-1.2.7])           | N/A                    | N/A

### Onion Service servers

Feature                                 | Specification                                | Status on [C Tor][]                            | Status on [Arti][]                                   | Config on [C Tor][]                                             | Config on [Arti][]
-------------------------------         | -------------                                | -------------------                            | ------------------                                   | -------------------                                             | ------------------
Load balancing                          | Unspecified[^balance-spec]                   | :white_check_mark: Implemented                 | Partially supported[^balance-partial]                | `HiddenServiceOnionBalanceInstance`; `MasterOnionAddress`       |
Vanguards (lite and full)               | [vanguards-spec][], [prop333][], [prop292][] | :white_check_mark: Implemented                 | :white_check_mark: Implemented ([1.2.2][arti-1.2.2]) | `VanguardsLiteEnabled`; full mode with [plugin][vanguards-plug] | `vanguards`
Restricted discovery[^restricted-disc]  | [rend-spec][]                                | :white_check_mark: Implemented                 | :white_check_mark: Implemented ([1.2.7][arti-1.2.7]) |                                                                 | `restricted_mode`
Introduction-point auth[^intro-auth]    | [Draft][intro-auth-draft]                    | :x: Unlikely                                   | :x: Not implemented                                  |                                                                 |
Stealth/hidden auth mode                | Unspecified[^stealth]                        | :x: Unlikely                                   | :x: Not implemented                                  |                                                                 |
Single Hop Mode                         | [prop260][]                                  | :white_check_mark: Implemented                 | [Planned][single-arti]                               | `HiddenServiceSingleHopMode`; `HiddenServiceNonAnonymousMode`   |
OOM DoS protections                     | [dos-spec][]                                 | :white_check_mark: Implemented                 | [Implemented][oom-arti] ([1.3.0][arti-1.3.0])        | `MaxMemInQueues`                                                | `system.memory`, `system.max_files`
Onion Services DoS protections          | [rend-spec][]                                | :white_check_mark: Implemented                 | Partially supported                                  | [Many][dos-protections-c]                                       | `max_concurrent_streams_per_circuit`
Proof of Work protection                | [prop327][]                                  | :white_check_mark: Implemented (0.4.8.1-alpha) | Planned[^pow-arti-server]                            | `HiddenServicePoW{DefensesEnabled,QueueRate,QueueBurst}`        |
Conflux                                 | [prop329][]                                  | [Planned][conflux-c-tor]                       | :x: Not implemented                                  | N/A                                                             | N/A
UNIX sockets[^sockets]                  | Unspecified                                  | :white_check_mark: Implemented                 | [Planned][sockets-arti]                              | `HiddenServicePort`                                             |
Observability and Metrics               | Unspecified                                  | :white_check_mark: Implemented                 | Planned[^metrics-arti]                              | `MetricsPort`; `MetricsPortPolicy`                              |
Circuit ID exporting                    | Unspecified                                  | :white_check_mark: Implemented                 | :x: Not implemented                                  | `HiddenServiceExportCircuitID`                                  |
CAA (ACME)                              | [prop343][]                                  | [Needs work][caa-c]                            | [Planned][caa-arti]                                  | `HiddenServiceCAA`                                              |
Offline Keys                            | [rend-spec][rend-spec-offline]               | :x: [Unlikely][offline-c]                      | [Planned][offline-arti]                              |                                                                 |
PKCS#11 tokens                          | [PKCS#11][]                                  | :x: Unlikely                                   | [Considered][pkcs-arti]                              |                                                                 |
CLI for key generation                  | Unspecified                                  | :white_check_mark: Third-party[^genkey-c]      | :white_check_mark: [Implemented][arti-genkeys] ([1.2.8][arti-1.2.8]) | N/A                                                             | N/A
Keys migration[^migration]              | Unspecified                                  | :x: Not implemented                            | :x: Not implemented                                  | N/A                                                             | N/A
Debian package[^package]                | [Debian Policy][]                            | :white_check_mark: [Implemented][debs]         | [Underway][artipack]                                 | N/A                                                             | N/A
Dockerfile                              | [dockerfile][]                               | :x: Not implemented                            | :x: Not implemented                                  | N/A                                                             | N/A
Official container image                | Unspecified                                  | :x: Not implemented                            | :x: Not implemented                                  | N/A                                                             | N/A

### Legend

* N/A: not applicable.
* Unspecified: this means there's no [tor-spec][] for the feature, which
  usually means it's implementation specific, and not standardized (yet).
* Draft: there's a draft spec not yet proposed.
* :x: Not implemented: the feature is not implemented, and still not planned.
* :x: Unlikely: it's unlikely that the feature will be implemented.
* Considered: feature is being considered, but not planned (yet).
* Planned: feature was added in the roadmap.
* Underway: feature is being actively developed, or (almost) ready
  for production.
* Needs work: there's initial code implementing the feature, but additional
  work is needed.
* Partially supported: feature has partial support, either by being partially
  implemented or by being leveraged by some property or third-party plugin.
* :white_check_mark: Third-party: functionality works through third-party tools
  or plugins.
* :white_check_mark: Implemented: feature has been fully implemented.

[Arti]: https://arti.torproject.org
[C Tor]: https://gitlab.torproject.org/tpo/core/tor
[tor-spec]: https://gitlab.torproject.org/tpo/core/torspec/
[debs]: https://deb.torproject.org/
[artipack]: https://gitlab.torproject.org/tpo/core/arti/-/merge_requests/2323
[vanguards-spec]: https://spec.torproject.org/vanguards-spec/
[vanguards-plug]: https://github.com/mikeperry-tor/vanguards
[prop260]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/260-rend-single-onion.txt
[prop292]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/292-mesh-vanguards.txt
[prop327]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/327-pow-over-intro.txt
[prop333]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/333-vanguards-lite.md
[prop255]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/255-hs-load-balancing.txt
[prop307]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/307-onionbalance-v3.txt
[prop343]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/343-rend-caa.txt
[prop329]: https://spec.torproject.org/proposals/329-traffic-splitting.html
[single-arti]: https://gitlab.torproject.org/tpo/core/arti/-/issues/727
[dos-spec]: https://gitlab.torproject.org/tpo/core/torspec/-/tree/main/spec/dos-spec
[oom-arti]: https://gitlab.torproject.org/tpo/core/arti/-/issues/351
[sockets-arti]: https://gitlab.torproject.org/tpo/core/arti/-/issues/1246
[rend-spec]: https://spec.torproject.org/rend-spec
[rend-spec-offline]: https://spec.torproject.org/rend-spec/protocol-overview.html#imd-offline-keys
[balance-naive]: https://gitlab.torproject.org/tpo/core/torspec/-/issues/155
[onionbalance]: ../apps/base/onionbalance/README.md
[control-spec]: https://spec.torproject.org/control-spec/
[dockerfile]: https://docs.docker.com/reference/dockerfile/
[Debian Policy]: https://www.debian.org/doc/debian-policy/
[balance-parallel]: ../../apps/web/onionspray/guides/balance/README.md#running-multiple-instances-in-parallel
[tpo/core/arti#1003]: https://gitlab.torproject.org/tpo/core/arti/-/issues/1003
[tpo/core/arti#1726]: https://gitlab.torproject.org/tpo/core/arti/-/issues/1726
[pow-arti-client]: https://gitlab.torproject.org/tpo/core/arti/-/merge_requests/2486
[service-side-pow.md]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/doc/dev/notes/service-side-pow.md
[tpo/core/arti#1667]: https://gitlab.torproject.org/tpo/core/arti/-/issues/1667
[caa-c]: https://gitlab.torproject.org/tpo/core/tor/-/merge_requests/716
[caa-arti]: https://gitlab.torproject.org/tpo/core/arti/-/issues/1414
[dos-protections-c]: https://community.torproject.org/onion-services/advanced/dos/
[offline-arti]: https://gitlab.torproject.org/tpo/core/arti/-/issues/1194
[tpo/core/tor#18098]: https://gitlab.torproject.org/tpo/core/tor/-/issues/18098
[tpo/core/arti#1028]: https://gitlab.torproject.org/tpo/core/arti/-/issues/1028
[Onionmine]: ../../apps/web/onionmine/README.md
[tpo/core/torspec#150]: https://gitlab.torproject.org/tpo/core/torspec/-/issues/150
[tpo/core/torspec#119]: https://gitlab.torproject.org/tpo/core/torspec/-/issues/119
[rend-spec-v2]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/attic/rend-spec-v2.txt?ref_type=heads
[offline-c]: https://gitlab.torproject.org/tpo/core/tor/-/issues/29054
[PKCS#11]: https://docs.oasis-open.org/pkcs11/pkcs11-spec/v3.1/cs01/pkcs11-spec-v3.1-cs01.html
[pkcs-arti]: https://gitlab.torproject.org/tpo/core/arti/-/issues/812
[arti-genkeys]: https://gitlab.torproject.org/tpo/core/arti/-/issues/1621
[arti-1.2.2]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md?ref_type=heads#arti-122--30-april-2024
[arti-1.2.7]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md?ref_type=heads#arti-127--3-september-2024
[arti-1.2.8]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md?ref_type=heads#arti-128--1-october-2024
[arti-1.3.0]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md?ref_type=heads#arti-130---31-october-2024
[client-auth]: https://community.torproject.org/onion-services/advanced/client-auth/
[intro-auth-draft]: https://gitlab.torproject.org/amjoseph/prop-onion-mutual-auth/
[conflux-c-tor]: https://gitlab.torproject.org/tpo/core/tor/-/issues/40716

[^package]: Debian package with Onion Services _server_ support.

[^sockets]: UNIX sockets sockets for proxying requests.

[^migration]: Keys migration from one implementation to the other.

[^balance-spec]: Although [prop255][] (Controller features to allow for
    load-balancing hidden services) and [prop307][] (Onion Balance Support for
    Onion Service v3) exists, it seems that the current
    [implementation][onionbalance] is not specified, relying only on the
    [control-spec][] built upon a [naive load balancing which depends
    on a faulty cross-certs in introduction point keys][balance-naive].
    It's also worth noting that any Onion Service implementation should
    support [running multiple parallel instances out-of-the-box][balance-parallel].

[^genkey-c]: According to [tpo/core/tor#18098][], this is unlikely to be
    implemented on [C Tor][], but can be achieved using tools such as
    [Onionmine][].

[^intro-auth]: This idea is discussed at [tpo/core/arti#1028][].

[^stealth]: While this was specified for [rend-spec-v2][] (2.2. Authorization
    for limited number of clients), this was never implemented, at least for
    Onion Services v3. Idea is further discussed on [tpo/core/torspec#119][]
    and [tpo/core/torspec#150][].

[^balance-partial]: "Partial" support here means that a simple load balancing
    functionality can be achieved by simply running an Onion Services in
    multiple parallel instances of [Arti][].

[^restricted-disc]: Restricted discovery is also known as [Client Authorization][client-auth].

[^metrics-arti]: This is planned on [tpo/core/arti#1003][] and on [tpo/core/arti#1726][].

[^pow-arti-server]: Tracked at [tpo/core/arti#1667][]. Check also
    [service-side-pow.md][] for implementation details.

## Key formats

Each implementation has it's own storage format for the keys ([C Tor][], [Onionbalance][] and [Arti][]).

Migration support right now is like the following.

!!! tip

    If unsure between the [C Tor][] and [Onionbalance][] formats, generate your
    keys in a program that uses the [C Tor][] format, and then load it on
    [Onionbalance][] as needed.

[Onionbalance]: ../../apps/base/onionbalance/README.md

### From C Tor to Onionbalance

* Works: [Onionbalance][] can load the keys in [C Tor][]'s format.
* References:
    * [Onionbalance v3 tutorial](../../apps/base/onionbalance/v3/tutorial.md#step-2-configuring-the-frontend-server-setting-up-onionbalance).
    * [onionbalance/hs_v3/service.py][]

[onionbalance/hs_v3/service.py]: https://gitlab.torproject.org/tpo/onion-services/onionbalance/-/blob/140cf1fcd5949a51968cfd5e1726d9b756d6c844/onionbalance/hs_v3/service.py#L105

### From Onionbalance to C Tor

* Not available.
* [Onionbalance][] format is defined at [onionbalance/config_generator/config_generator.py][].
* It seems that the [C Tor][] key storage format is not standardized/specified
  anywhere, but it's defined at [src/lib/crypt_ops/crypto_ed25519.c][].
* This is handled by [tpo/onion-services/onionbalance#35][].

[onionbalance/config_generator/config_generator.py]: https://gitlab.torproject.org/tpo/onion-services/onionbalance/-/blob/140cf1fcd5949a51968cfd5e1726d9b756d6c844/onionbalance/config_generator/config_generator.py#L283
[src/lib/crypt_ops/crypto_ed25519.c]: https://gitlab.torproject.org/tpo/core/tor/-/blob/a56350abc8a8c18bfe0b60c751cce5f851db0d3e/src/lib/crypt_ops/crypto_ed25519.c#L553
[tpo/onion-services/onionbalance#35]: https://gitlab.torproject.org/tpo/onion-services/onionbalance/-/issues/35

### From C Tor to Arti

* Partially works:
  * [Arti can use existing C Tor keys][].
  * [Arti][] uses the [SSH format](https://spec.torproject.org/ssh-protocols.html).
  * Migrating keys from C Tor to Arti's format is still not available, but
    needs to be implemented at some point, so Onion Service Operators can fully
    migrate their services to [Arti][].

[Arti can use existing C Tor keys]: https://gitlab.torproject.org/tpo/core/arti/-/issues/858

### From Arti to C Tor

* Not available.
* Probably won't be implemented.

### From Onionbalance to Arti

* Not available, but needs to be implemented at some point so Onion Service
  Operators can migrate their services to [Arti][].

### From Arti to Onionbalance

* Not available.
* Probably won't be implemented.

## Notes
