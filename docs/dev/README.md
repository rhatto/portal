# Development

This section has basic information about how to develop applications with
buil-in Onion Services support, and it's intended to be replace in the future
with a proper section in the upcoming [Tor Developer Portal][].

<div class="grid cards" markdown>
-   :onion:{ .lg .middle } __Cebollitas__

    ---

    If you're looking for simple, beginner examples about Onion Services
    integration, check the Cebollitas project.

    [:octicons-arrow-right-24: Cebollitas][Cebollitas]

-   :material-remote-desktop:{ .lg .middle } __Onion Desktop__

    ---

    Another interesting integration project example is the Onion Desktop project.

    [:octicons-arrow-right-24: Onion Desktop][Onion Desktop]

-   :material-application-cog-outline:{ .lg .middle } __Implementations__

    ---

    Status and features of existing Onion Service implementations, useful for
    operators and developers.

    [:octicons-arrow-right-24: Implementations docs](implementations/README.md)

-   :octicons-tools-24:{ .lg .middle } __Libraries__

    ---

    Developers might be interested in know about the existing libraries for
    integrating Onion Services into their applications.

    [:octicons-arrow-right-24: Libraries docs](libraries/README.md)

</div>

[Tor Developer Portal]: https://gitlab.torproject.org/groups/tpo/-/milestones/23
[Cebollitas]: https://gitlab.torproject.org/tpo/onion-services/cebollitas
[Onion Desktop]: https://gitlab.torproject.org/tpo/onion-services/onion-desktop
