# Technology

[Onion Services][] is a communication technology for exchanging data using the
[Tor network][], offering many [security and availability
properties](properties.md).

This section has further discussions about how it works, it's features,
security characteristics and specifications.

Check also the [Introduction to Onion Services][] slide deck <!--(from 2022)-->
for a technology and applications overview.

[Onion Services]: https://community.torproject.org/onion-services/
[Tor network]: https://2019.www.torproject.org/about/overview.html.en
[Introduction to Onion Services]: https://gitlab.torproject.org/tpo/community/training/-/raw/master/2022/onion-services/intro-onion-service-2022.pdf
[overview]: https://community.torproject.org/onion-services/overview/
[rend-spec]: https://spec.torproject.org/rend-spec/index.html

<div class="grid cards" markdown>

-   :onion:{ .lg .middle } __Overview__

    ---

    A quick glance on how Onion Services works.

    [:octicons-arrow-right-24: Onion Services overview][overview]

-   :material-feature-search:{ .lg .middle } __Properties__

    ---

    Onion Services have many interesting and useful properties.

    [:octicons-arrow-right-24: Onion Services properties](properties.md)

-   :material-timeline:{ .lg .middle } __Timeline__

    ---

    Check the most relevant events in the Onion Services history.

    [:octicons-arrow-right-24: Onion Services timeline](timeline.md)

-   :book:{ .lg .middle } __Terminology__

    ---

    Quick terminology guide for Onion Services.

    [:octicons-arrow-right-24: Terminology and glossary](terminology.md)

-   :material-security:{ .lg .middle } __Security__

    ---

    Onion Services Security introduction.

    [:octicons-arrow-right-24: Security Overview](security/README.md)

-   :material-pencil-ruler:{ .lg .middle } __Specification__

    ---

    Onion Services is defined by the Tor Rendezvous Specification.

    [:octicons-arrow-right-24: Onion Services specs][rend-spec]

</div>
