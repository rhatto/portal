# Other

Other applications using Onion Services.

## Conferencing

* [Wahay](https://wahay.org/): an easy-to-use, secure and decentralized
  conference call application.

## Email

* [ehloonion/onionmx: Onion delivery, so delicious](https://github.com/ehloonion/onionmx/)
