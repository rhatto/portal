# Whistleblowing

Whistleblowing _is the activity of a person, often an employee, revealing
information about activity within a private or public organization that is
deemed illegal, immoral, illicit, unsafe or fraudulent_
([source](https://en.wikipedia.org/wiki/Whistleblowing)).

The Onion Services technology is very important to ensure whistleblowers' safety,
and are built in the following platforms:

* [GlobaLeaks - Free and Open-Source Whistleblowing Software](https://www.globaleaks.org/)
* [SecureDrop - Share and accept documents securely](https://securedrop.org/)
* [Hush Line](https://hushline.app/): a lightweight, secure, and anonymous tip line.
